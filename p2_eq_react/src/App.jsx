import { useState } from 'react'
import './App.css'
import Compo1 from "./Components/Compo1"
import Compo2 from './Components/Compo2'
import Compo3 from './Components/Compo3'
import Compo6 from './Components/Compo6'
import Compo7 from './Componente/Compo7'
import Comp1 from './Componentes/Comp1'
import Comp2 from './Componentes/Comp2'
import Comp3 from './Componentes/Comp3'
import Comp4 from './Componentes/Comp4'


function App() {

  return (
    <>
      <body>
        <Compo1></Compo1>
        <div class="d2">
          <Compo2></Compo2>
          <Compo3></Compo3>
          <Compo6></Compo6>
        </div>
        <br/>
        <div className="d7">
          <Compo7/>
        </div>
        <Comp1></Comp1>
        <div class="d10">
          <Comp2></Comp2>
          <Comp3></Comp3>
          <Comp4></Comp4>
        </div>
        
      </body>
      
    </>
  )
}

export default App
