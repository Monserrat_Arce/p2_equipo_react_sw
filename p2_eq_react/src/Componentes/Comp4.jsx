import comp4 from '../Componentes/comp4.css'
import img2 from '../assets/img3/img2.jpg'


function Comp4(){
    return(
        <>
            <div class="cards" >
                <div class="cente">
                    <img src={img2} class="card-img-to" style={{width: '150px', height: '140px'}}/>
                </div>

                <div class="card-bodi">
                    <div class="card-heade">
                        <h5 class="card-titl">Gustos personales</h5>
                    </div>
                    <p class="card-text">Deporte favorito:  Fútbol</p>
                    <p class="card-text">Animal favorito: Tigre.</p>
                    <p class="card-text">Lugar favorito: Rios</p>
                    <p class="card-text">Personas favoritas:  Familia</p>
                    <p class="card-text">Videojuegos: Call Of Duty</p>
                </div>
            </div>
        </>
    )
}
export default Comp4