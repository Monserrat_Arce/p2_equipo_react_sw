import estudiantes from '../assets/img/estudiantes.png'
import compo3 from '../components/compo3.css'

function Compo3(){
    return(
        <>
            <div class="card" >
                <div class="center">
                    <img src={estudiantes} class="card-img-top" style={{width: '150px', height: '140px'}}/>
                </div>

                <div class="card-body">
                    <div class="card-header">
                        <h5 class="card-title">Información estudiantil</h5>
                    </div>
                    <p class="card-text">No.Control:  19680098.</p>
                    <p class="card-text">Correo electronico: 19680098@cuautla.tecnm.mx.</p>
                    <p class="card-text">Carrera: Ing. Sistemas Computacinales</p>
                    <p class="card-text">Semestre:  8vo.</p>
                </div>
            </div>
        </>
    )
}
export default Compo3